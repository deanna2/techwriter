# Subject matter experts (SMEs)

Here's a list of the four subject matter experts on the team:

**Craig C**

Has expertise in: 

* Java
* Laptop imaging process
* The instructor experience (was one of the first instructors at TE)

**Dave Bry**

Has expertise in: 

* C#/.NET
* Curriculum development process (contributed to developing team handbook, oversees the end-of-sprint process)

**David P-C**

Has expertise in: 

* Java
* The instructor experience (transitioned from instructor to curriculum developer)

**Mike M**

Has expertise in: 

* C#/.NET
* The instructor experience (transitioned from instructor to curriculum developer)
