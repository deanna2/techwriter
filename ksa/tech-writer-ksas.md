# KSA for technical writer

| Knowledge, skill, or ability | Importance to role | 
| :--------------------------- | :----------------- |
| Reviewing materials submitted by software developers for clarity, punctuation, grammar, content, formatting | Used every day. Technical writer should be well-versed in English grammar and technical writing best practices. |
| Collaborating with software developers to complete revisions, work through a documentation bug, or learn more about an application or technical concept | Used regularly. Technical writer shouldn’t be afraid to follow up with developers to get the information they need and ask well-informed questions. |
| Maintaining our documentation linter, troubleshooting errors, knowledge of how to build, maintain, and modify custom rules | Used regularly. This is acquired knowledge and will require that the technical writer read all the documentation for the linter and probably demonstrate some kind of competence in this area. |
| Designing graphics, like ERDs, process diagrams, other technical diagrams | Used every now and then. The technical writer should be familiar with or be willing to learn industry-standard tools for designing graphics like Snagit, maybe Photoshop, Microsoft Visio, or draw.io. |
| Using a style guide and managing style guide development | Used regularly. Ideally, the technical writer will have some experience working with and contributing to style guides. |
| Being able to read and understand code | Used regularly. It’s hard to make quality edits to a tutorial or the student book without being able to read code samples. Ideally, they're familiar with *at least* one OOP language.  |
| Working in a docs as code environment | Used regularly. Ideally, the technical writer has some experience working in a docs as code environment (Markdown + some SSG + GitLab/GitHub pages or Netlify). 
| Git | Used every day. The technical writer should ideally come into the role knowing Git already. |
| Understanding the Learn ecosystem | Used regularly and is acquired knowledge. Will probably need to go through the same Learn training Erin provided earlier this year. |
| Writing and publishing release notes | Used regularly. The technical writer needs to write clear descriptions of the work completed for the release notes and communicate updates to instructors. |