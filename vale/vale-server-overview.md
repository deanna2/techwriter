# Vale Server 

To make sure our documentation adheres to style guide guidelines, we use a tool called Vale Server. Vale Server is a cross-platform (Windows, macOS, and Linux) desktop application and is built on top of, and offers a high degree of compatibility with, the [Vale command-line tool](https://docs.errata.ai/vale/about/).

The application consists of two parts: a server (which manages Vale-related configuration details) and a client (which displays Vale's results).

Before you can start using Vale Server, you need to read the [Documentation testing](https://te-academics-developers.gitlab.io/style-guide/documentation/testing.html#documentation-testing) section of the style guide. It provides an overview of Vale Server, how to install it, and how to use it.