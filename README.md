# Tech writer repository

This repository contains several resources for technical writers at Tech Elevator, including:

* A Knowledge, Skill, and Ability (KSA) analysis 
* An editing checklist
* A list of subject matter experts 
* Guidelines for writing release notes 
* A guide to creating and managing our documentation linter


