# Self-edit checklist

Use this checklist when editing documentation. Keep in mind that this isn't an exhaustive list. Make sure to check the [Tech Elevator curriculum style guide](https://te-academics-developers.gitlab.io/style-guide/) *and* the [Google Developer Documentation Style Guide](https://developers.google.com/style) for guidance.

## Readability

- [ ] [Shorter sentences and paragraphs](https://te-academics-developers.gitlab.io/style-guide/documentation/general-writing-guidelines.html#shorter-sentences-and-paragraphs) are used
- [ ] Headings are used to break up large sections of text 

## Content

- [ ] Terms and technical concepts are clearly explained
- [ ] Code samples have context (what does this code sample do? what does it show? never drop a code sample in the middle of prose)
- [ ] Code samples are accurate, typo-free, and code adheres to [style guide conventions](https://te-academics-developers.gitlab.io/style-guide/)

## Accessibility

- [ ] There's no directional language like "above" or "below" when referring to a position in a document
- [ ] Graphics have captions

## Voice and tone 

- [ ] [Wordy phrases](https://te-academics-developers.gitlab.io/style-guide/documentation/general-writing-guidelines.html#concise-terms) have been tightened
- [ ] Unnecessary words (like modifiers) have been deleted 
- [ ] There are no jokes, pop-culture references, or use of self-deprecating humor
- [ ] [Voice and tone](https://developers.google.com/style/tone) is conversational, friendly, approachable

## Formatting

- [ ] UI elements and terms are **bolded**
- [ ] Anything that needs to be emphasized is in *italics*, not bold, bold-italic, or CAPS
- [ ] Callouts adhere to [style guide conventions](https://te-academics-developers.gitlab.io/style-guide/documentation/images.html#using-callouts)
- [ ] Multi-action procedures use **>**, not **->**
- [ ] Steps that must be completed in order use [*numbered lists*](https://te-academics-developers.gitlab.io/style-guide/documentation/general-writing-guidelines.html#lists)
- [ ] Code font is used appropriately 

## Grammar and copy edits

- [ ] There are no typos 
- [ ] Spelling of "suspect" words is correct (think affect vs. effect, its vs. it's)
- [ ] There's minimal use of [passive voice](https://te-academics-developers.gitlab.io/style-guide/documentation/general-writing-guidelines.html#active-and-passive-voice)