# Release notes guidelines (v2.4) 

This document provides guidelines for writing release notes for version 2.4 of the curriculum. Once we roll out v3.0, this process will change.

## Structure

Use the following main sections in the release notes:

* Version number and date
* Release Highlights
* New Content
* Major Enhancements
* Minor Enhancements
* Bug Fixes

> If one of these sections has no content for a release, don't include it in the release notes. 

## Formatting

* Use title case for section headings
* Use bulleted lists, not paragraphs, for each task
* Start each phrase with a past tense verb ("Added...", "Modified...", "Resolved...")
* Include a link to the card in Asana for each task
* Include the week and day for each task 

## Wording

Use the following guidelines when creating items in each section. 

| Section | Guideline | 
| :------ | :-------- | 
| Release Highlights | Include the most important features, fixes, or enhancements for the release. |
| New Content | Provide a few sentences that describe the new content. If necessary, add more details in the form of a bulleted list. |
| Major Enhancements | Provide a few sentences that describe the major enhancements. If necessary, add more details in the form of a bulleted list. | 
| Minor Enhancements | Use a short sentence to describe the enhancement. | 
| Bug Fixes | Use a short sentence to describe the bug that was fixed. | 

See the [v2.2 Release Notes](https://bitbucket.org/te-curriculum/te-curriculum/src/release_v2_4/release-notes/v2_2-release-notes.md) as an example of what the release notes should look like. 

## Process for writing release notes

To write the release notes, you need to go through the "Done" section in the Kanban to get the title, task type, and link for each task.

> From Deanna: I've been using a script that generates the release notes for me. It isn't perfect at all, but it reduces the manual work of going through each task in the "Done" section. You can see the script in my [GitLab repository](https://gitlab.com/deanna2/generate-release-notes). Feel free to use it, modify it, or don't use it at all—up to you.

### Tips for writing 

* Include the language the enhancement or fix is for (Java, .NET, or both)
* Review the pull request to understand what changes were made, if needed
* Keep descriptions short and to the point
* Go through the "Done" section after drafting to make sure you haven't missed any tasks (sometimes they aren't tagged correctly or assigned a task type)
